/*
 * $File: image_fusion.hh
 * $Date: Tue May 13 13:22:20 2014 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "image.hh"
#include "point.hh"

// anti-clockwise
static const std::vector<std::pair<int, int>> NEIGHBOR_DIRS = {
	{0, 1}, {-1, 0}, {0, -1}, {1, 0}
};

class ImageFusion {
	public:
		enum FusionType {
			IMPORTING_GRADIENT,
			MIXING_GRADIENT
		};

		/**
		 * Fusion two images together, given position, mask and fusion type.
		 */
		cv::Mat fusion(
				cv::Mat base_img,
				cv::Mat target_img,
				cv::Mat target_img_mask,
				Point up_left_corner,
				FusionType ftype);

	private:
		typedef ImageBase<int> IdImage;

		/**
		 * poisson image editing framework.
		 * @param vector_field vector field v_{pq} of p, contains 4 number, denote
		 *		magnitute of vector field in anti-clockwise order. See @NEIGHBOR_DIRS.
		 */
		void poisson_image_editing(
				const MonoImage &base_img,
				const MonoImage &target_mask,
				const ImageBase<std::vector<double>> &vector_field,
				const Point &up_left_corner,
				MonoImage &output);



		void fill_boundary_by_mask(const MonoImage &target_mask, MonoImage &boundary_mask);

		/**
		 * return number of non-zero pixels
		 */
		int fill_id_by_mask(
				const MonoImage &base_img,
				const MonoImage &mask_img,
				const Point &up_left_corner,
				IdImage &mask_id,
				std::vector<Point> &mask_valid_pos);
};

/*
 * vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
 */
