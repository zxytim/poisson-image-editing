/*
 * $File: image.hh
 * $Date: Tue May 13 00:55:33 2014 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#pragma once

#include <vector>

/**
 * A single channel image class wrapper, implemented
 * using vector
 */
template<class pixel_t>
class ImageBase {
	public:

		std::vector<pixel_t> data;

		int size() const { return m_rows * m_cols; }
		ImageBase &reshape(int rows, int cols) {
			m_rows = rows;
			m_cols = cols;
			data.resize(size());
			return *this;
		}

		ImageBase &fill(const pixel_t &content) {
			int s = size();
			for (int i = 0; i < s; i ++)
				data[i] = content;
			return *this;
		}


		int rows() const { return m_rows; }
		int cols() const { return m_cols; }

		pixel_t &get(int x, int y) {
			assert(x >= 0 && x < m_rows && y >= 0 && y < m_cols);
			return data[x * m_cols + y];
		}

		const pixel_t &get(int x, int y) const {
			assert(x >= 0 && x < m_rows && y >= 0 && y < m_cols);
			return data[x * m_cols + y];
		}

		ImageBase(int rows = 0, int cols = 0) : m_rows(rows), m_cols(cols) {
			data.resize(rows * cols);
		}

	private:
		int m_rows;
		int m_cols;
};

typedef ImageBase<unsigned char> MonoImage;

/*
 * vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
 */

