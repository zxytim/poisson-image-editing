/*
 * $File: point.hh
 * $Date: Tue May 13 10:50:48 2014 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#pragma once

/**
 * A simple point implementation
 */
class Point {
	public:
		int x, y;
		Point(int x = 0, int y = 0): x(x), y(y) {}
};


/*
 * vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
 */

