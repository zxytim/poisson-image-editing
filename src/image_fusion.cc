/*
 * $File: image_fusion.cc
 * $Date: Wed May 14 00:56:04 2014 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "image_fusion.hh"
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstdarg>

#include "Eigen/Sparse"
#include "Eigen/Dense"

#include "timer.hh"

using namespace std;
using namespace Eigen;

#define IN_RANGE(x, y, rows, cols) ((x) >= 0 && (x) < (rows) && (y) >= 0 && (y < (cols)))
static const int NO_FIELD = 100000;

static const int MASKED = 255;
static const int UNMAKED = 0;
static inline int clip(double v) {
	return min(255.0, max(0.0, v));
}

static inline bool is_masked(int v) {
	return v >= 256/2;
}

static inline void lprintf(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fflush(stderr);
}


void ImageFusion::poisson_image_editing(
		const MonoImage &base_img,
		const MonoImage &target_mask,
		const ImageBase<vector<double>> &vector_field,
		const Point &up_left_corner,
		MonoImage &output) {
	assert(target_mask.rows() == vector_field.rows());
	assert(target_mask.cols() == vector_field.cols());

	MonoImage boundary_mask;
	fill_boundary_by_mask(target_mask, boundary_mask);

	int rows = target_mask.rows(),
		cols = target_mask.cols();

	int start_x = up_left_corner.x,
		start_y = up_left_corner.y;

	IdImage id_of_target;
	vector<Point> mask_valid_pos;
	int N = fill_id_by_mask(base_img, target_mask, up_left_corner,
			 id_of_target, mask_valid_pos);

	vector<Triplet<double>> triplets;
	SparseMatrix<double> A(N, N);
	VectorXd b(VectorXd::Zero(N));

	int eqn_id = 0;
	lprintf("building equation system ...\n");
	// build the equation system
	for (int i = 0; i < rows; i ++)
		for (int j = 0; j < cols; j ++) {
			if (!IN_RANGE(start_x + i, start_y + j, base_img.rows(), base_img.cols()))
				continue;
			if (!is_masked(target_mask.get(i, j)))
				continue;

			int id0 = id_of_target.get(i, j);
			assert(id0 != -1);

			auto &v = vector_field.get(i, j);
			assert(v.size() == 4);

			int nr_neighbor = 0;
			double b_tmp = 0;
			for (int d = 0; d < 4; d ++) {
				auto &dir = NEIGHBOR_DIRS[d];
				int x = i + dir.first,
					y = j + dir.second;
				if (!IN_RANGE(x, y, rows, cols))
					continue;
				if (!IN_RANGE(start_x + x, start_y + y, base_img.rows(), base_img.cols()))
					continue;

				if (is_masked(target_mask.get(x, y))) { // in Omega
					int id1 = id_of_target.get(x, y);
					assert(id1 != -1);
					triplets.emplace_back(eqn_id, id1, -1);  // fill mat
				}

				if (is_masked(boundary_mask.get(x, y))) // in partial Omega
					b_tmp += base_img.get(start_x + x, start_y + y);

				double b_v = -vector_field.get(i, j)[d];
				assert(b_v != NO_FIELD);
				b_tmp += b_v;
				nr_neighbor ++;
			}
			triplets.emplace_back(eqn_id, id0, nr_neighbor); // fill mat
			b[eqn_id] = b_tmp;
			eqn_id ++;
		}

	assert(eqn_id == N);

	VectorXd f;
	{
		lprintf("solving equation ...\n");
		GuardedTimer timer("solve equation");
		// solving equations
		A.setFromTriplets(triplets.begin(), triplets.end());
		BiCGSTAB<SparseMatrix<double>, IncompleteLUT<double>> solver;
		solver.setMaxIterations(10000);
		solver.compute(A);
//        SparseQR<SparseMatrix<double>,  COLAMDOrdering<int>> solver(A);
		f = solver.solve(b);
		lprintf("%g\n", (A * f - b).cwiseAbs().sum());
	}

	// construct output
	lprintf("constructing output ...\n");
	output = base_img;
	int cnt = 0;
	for (int i = 0; i < rows; i ++)
		for (int j = 0; j < cols; j ++) {
			if (!is_masked(target_mask.get(i, j)))
				continue;
			if (!IN_RANGE(start_x + i, start_y + j, base_img.rows(), base_img.cols()))
				continue;
			cnt ++;

			double val = f[id_of_target.get(i, j)];
			int orig = output.get(start_x + i, start_y + j);
//            lprintf("%d %d %d %f\n", i, j, orig, val);
			output.get(start_x + i, start_y + j) = clip(val);
//            lprintf("%d %f %d %f %f\n", orig, val, clip(val), val - clip(val), val - orig);
		}
	lprintf("##### %d %d #####\n", N, cnt);
}


int ImageFusion::fill_id_by_mask(
		const MonoImage &base_img,
		const MonoImage &mask_img,
		const Point &up_left_corner,
		IdImage &mask_id,
		vector<Point> &mask_valid_pos) {

	mask_valid_pos.resize(0);
	mask_id.reshape(mask_img.rows(), mask_img.cols()).fill(-1);

	auto &s = up_left_corner;
	int cnt = 0;
	for (int i = 0; i < mask_img.rows(); i ++)
		for (int j = 0; j < mask_img.cols(); j ++) {
			if (!is_masked(mask_img.get(i, j)))
				continue;
			if (!IN_RANGE(s.x + i, s.y + j, base_img.rows(), base_img.cols()))
				continue;
			mask_valid_pos.emplace_back(i, j);
			mask_id.get(i, j) = cnt ++;
		}
	return cnt;
}

void ImageFusion::fill_boundary_by_mask(
		const MonoImage &target_mask,
		MonoImage &boundary_mask) {
	int rows = target_mask.rows(),
		cols = target_mask.cols();
	boundary_mask.reshape(rows, cols);
	for (int i = 0; i < rows; i ++)
		for (int j = 0; j < cols; j ++) {
			if (is_masked(target_mask.get(i, j)))
				continue;

			// we have target_mask[i][j] == 0 now
			for (auto &dir: NEIGHBOR_DIRS) {
				int x = i + dir.first,
					y = j + dir.second;
				if (!(x >= 0 && x < rows && y >= 0 && y < cols))
					continue;
				if (is_masked(target_mask.get(x, y))) {
					boundary_mask.get(i, j) = MASKED;
					break;
				}
			}
		}
}


static inline void Mat2MonoImages(
		cv::Mat mat, vector<MonoImage> &imgs) {
	assert(mat.depth() == CV_8UC1);

	int channels = mat.channels();
	assert(channels == 1 || channels == 3);
	int rows = mat.rows,
		cols = mat.cols;
	imgs.resize(channels);
	for (auto &img: imgs)
		img.reshape(rows, cols);
	for (int i = 0; i < rows; i ++)
		for (int j = 0; j < cols; j ++) {
			for (int k = 0; k < channels; k ++) {
				int p = 0;
				if (channels == 1)
					p = mat.at<uchar>(i, j);
				else
					p = mat.at<cv::Vec3b>(i, j)[k];
				imgs[k].get(i, j) = p;
			}
		}
}

template<class A, class B>
static inline bool same_size(const A &a, const B &b) {
	return a.rows() == b.rows() && a.cols() == b.cols();
}


static inline cv::Mat MonoImages2Mat(const vector<MonoImage> &imgs){
	assert(imgs.size() > 0);
	for (int i = 0; i < ((int)imgs.size()) - 1; i ++)
		assert(same_size(imgs[i], imgs[i + 1]));
	int channels = imgs.size();
	assert(channels == 1 || channels == 3);
	int rows = imgs[0].rows(),
		cols = imgs[0].cols();
	cv::Mat mat;
	if (channels == 1)
		mat = cv::Mat(rows, cols, CV_8UC1);
	else mat = cv::Mat(rows, cols, CV_8UC3);

	for (int k = 0; k < channels; k ++) {
		for (int i = 0; i < rows; i ++)
			for (int j = 0; j < cols; j ++) {
				int val = imgs[k].get(i, j);
				if (channels == 1)
					mat.at<uchar>(i, j) = val;
				else
					mat.at<cv::Vec3b>(i, j)[k] = val;
			}
	}

	return mat;
}

cv::Mat ImageFusion::fusion(
		cv::Mat base_img,
		cv::Mat target_img,
		cv::Mat target_img_mask,
		Point up_left_corner,
		FusionType ftype) {

	int channels = base_img.channels();

	vector<MonoImage>
		base_img_vec,
		target_img_vec,
		target_img_mask_vec_;

	Mat2MonoImages(base_img, base_img_vec);
	Mat2MonoImages(target_img, target_img_vec);
	Mat2MonoImages(target_img_mask, target_img_mask_vec_);

	assert((int)base_img_vec.size() == channels);
	assert((int)target_img_vec.size() == channels);
	assert(target_img_mask_vec_.size() == 1);
	assert(same_size(target_img_mask_vec_[0], target_img_vec[0]));


	MonoImage &target_img_mask_img = target_img_mask_vec_[0];
	int rows = target_img_mask_img.rows(),
		cols = target_img_mask_img.cols();
	int start_x = up_left_corner.x,
		start_y = up_left_corner.y;

	vector<MonoImage> outputs;
	for (int k = 0; k < channels; k ++) {
		auto &target = target_img_vec[k];
		auto &base = base_img_vec[k];
		ImageBase<vector<double>> vector_field(rows, cols);
		for (int i = 0; i < rows; i ++)
			for (int j = 0; j < cols; j ++) {
				vector_field.get(i, j) = vector<double>(4, NO_FIELD);
				for (int d = 0; d < 4; d ++) {
					auto &dir = NEIGHBOR_DIRS[d];
					int x = i + dir.first,
						y = j + dir.second;

					if (!IN_RANGE(x, y, rows, cols))
						continue;
					double vpq = 0;
					switch (ftype) {
						case IMPORTING_GRADIENT:
							vpq = (double)target.get(x, y) - (double)target.get(i, j);
							break;
						case MIXING_GRADIENT:
							{
								double v0 = (double)target.get(x, y) - (double)target.get(i, j);
								double v1 = (double)base.get(start_x + x, start_y + y)
									- (double)base.get(start_x + i, start_y + j);
								if (abs(v0) > abs(v1))
									vpq = v0;
								else vpq = v1;
							}
							break;
						default:
							assert(0);
					}
					vector_field.get(i, j)[d] = vpq;
				}
			}
		outputs.emplace_back();
		lprintf("poisson editing channel %d ...\n", k);
		poisson_image_editing(base, target_img_mask_img, vector_field, up_left_corner, outputs.back());
	}

	return MonoImages2Mat(outputs);
//    return MonoImages2Mat(vector<MonoImage>(1, outputs[2]));
}



/*
 * vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
 */

