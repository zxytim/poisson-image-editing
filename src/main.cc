/*
 * $File: main.cc
 * $Date: Sun May 18 12:46:24 2014 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include "include/image_fusion.hh"

#include <cstdlib>

int main(int argc, char *argv[]) {
	if (argc != 8) {
		printf("Usage: %s <base_img> <target_img> <target_img_mask> <up_left_x> <up_left_y> <fusion_type> <output>\n", argv[0]);
		printf("    <fusion_type> is one of 'importing_gradient' or 'mixing_gradient'\n");
		exit(1);
	}
	const char
		*base_img_path = argv[1],
		*target_img_path = argv[2],
		*target_img_mask_path = argv[3];
	Point up_left_corner(atoi(argv[4]), atoi(argv[5]));

	ImageFusion::FusionType ftype = ImageFusion::MIXING_GRADIENT;
	if (strcmp(argv[6], "importing_gradient") == 0)
		ftype = ImageFusion::IMPORTING_GRADIENT;
	printf("ftype: %s\n", ftype == ImageFusion::IMPORTING_GRADIENT ? "importing_gradient" : "mixing_gradient");

	cv::Mat
		base_img = cv::imread(base_img_path, CV_LOAD_IMAGE_COLOR),
		target_img = cv::imread(target_img_path, CV_LOAD_IMAGE_COLOR),
		target_img_mask = cv::imread(target_img_mask_path, CV_LOAD_IMAGE_GRAYSCALE);

	ImageFusion image_fusion;
	cv::Mat rst = image_fusion.fusion(
			base_img, target_img, target_img_mask,
			up_left_corner, ftype);

	cv::imwrite(argv[7], rst);

	cv::imshow("base_img", base_img);
	cv::imshow("target_img", target_img);
	cv::imshow("target_img_mask", target_img_mask);
	cv::imshow("result", rst);

	cv::waitKey(0);

}


/*
 * vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
 */

